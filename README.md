# 3D PointCloud into 2D Image

Reproject a 3D Point Cloud into a 2D image.  This code allows the generation of 2D images with the spatial information contained in the 3D point clouds. This simplified version is made to work directly with Metashape (Agisoft).

# Table of Contents:

---
1. [**Info & Basics**](#Info&Basics)
2. [**Data preparation**](#data)
3. [**Tutorial**](#tutorial)
4. [**Citation**](#citation)
---
<a name="Info&Basics"></a>
# Info & Basics
This project is still developing further!

If you are using this project. Please say "Hello": 
[Xabier Blanch, TU Dresden](https://xabierbg.eu/)

### Aim of the project
The aim of this project is to reproject points from a 3D Point Cloud into a 2D image (based on Metashape workflow). 
This algorithm allows the transfer of the information contained in the **3D point cloud** to the **2D image**. Or the 
opposite process, transferring the information contained in the **2D images** to the **3D point cloud**. 

This code is designed to be used with data extracted directly from Agisoft Metashape (the user only has to make 
minor modifications to the Metashape configuration). Since the algorithm is based on camera position, no control 
points (GCP) are needed to perform the reprojection of the 3D points into the 2D image. 

The quality of the result is directly linked to the quality of the processing carried out in Agisoft Metashape, both 
in the elaboration of the **point cloud**, the estimation of the **camera location** and the **calibration 
parameters** obtained.

Only the following input data are required:
* Point cloud (3D information)
* Image (2D information)
* Internal camera geometry (file obtained in Agisoft Metashape)
* External geometry of the camera (estimation made by Agisoft Metashape)

The result is a .csv file that stores the 3D points of the point cloud linked to the 2D coordinates of the image used. In addition, two images are stored to "check" the quality and accuracy of the result.

Image 1 (img1_file.png) shows the point cloud "projected" from the centre of the camera. This means that it shows the point cloud "viewed" through the camera.

Image 2 (img2_file.png) shows only the part of the point cloud that intersects the image, and the points are projected on top of it. This allows to see the accuracy of the reprojection.

# Data preparation
<a name="data"></a>
<details>
<summary><b><u>Click to expand</u></b></summary>
This section describes the input files needed to run the script. Please follow all the steps, and make sure that the files used are as described below.

By default, the code is configured to run the example **Set 1** from the "Example" folder. Enabling lines 20 and 21 
will set the code to read the files in the input_data folder as described in the structure detailed in this section. The output folder will be created automatically by the script.

#### Point cloud (3D information)
- [ ] File .txt obtained by Metashape Agisoft (or other similar software)
- [ ] Separator = ' ' (space separator)
- [ ] Attributes -> [x y z r g b]
- [ ] RGB values -> normalized (between 0 an 1)
- [ ] Don't use headers
#### Image (2D information)
- [ ] JPEG file 
####  Internal camera geometry (file obtained in Agisoft Metashape)
- [ ] File .xml obtained by Metashape Agisoft
- [ ] Export the adjusted values for your camera
- [ ] In the **Save as** window, change the **Save as type** to -> OpenCV Camera Calibration (*.xml)
#### External geometry of the camera (estimation made by Agisoft Metashape)
- [ ] Values obtained in the Reference tab in Agisoft Metashape
- [ ] It's important to change the default angles provided by Agisoft Metashape. Instead of Yaw, Pitch and Roll use 
  -> Omega, Phi, Kappa
- [ ] Copy only the estimated values East est (m), North est (m), Alt. est (m), Omega est (º), Phi est (º), Kappa est (º)
- [ ] If you have to work with a batch of images, you can export these values as a .txt file

Here is an example of the folder structure (with validation and test data):

```
3DPC_into_2DIMG/
    └─── input_data
    │     └─── Image file (2D info)
    │     └─── Point Cloud (3D info)
    │     │     └─── type: [x y z r g b] (normalized rgb -> [0-1])
    │     │     └─── format: .txt    
    │     └─── Camera Calibration file
    │            └─── type: OpenCV
    │            └─── format: .XML
    └─── output_data
          └─── Img1_file.png
          └─── Img2_file.png
          └─── file.csv
                  └─── type: [x_img, y_img, d, x_pc, y_pc, z_pc, r, g, b]    
```
</details>

# Tutorial (under construction)
<a name="tutorial"></a>
<details>
<summary><b><u>How to run the code</u></b></summary>

* Use Python 3.10, ensure pip and venv are installed!


* Install requirements.txt (file in folder 3D_into_2D)
  ```
  pip install -r 3D_into_2D/requirements.txt
  ```
* Modify **CHANGE INPUT/OUTPUT FOLDER** section. By default this section use files in Example Set 1 


* Introduce **INPUT FILE** section. By default this section shows Example Set 1


* Introduce **INPUT PARAMETERS** section. By default this section shows Example Set 1

  <details>
  <summary><b><u>Parameters Example Set 1</u></b></summary>

  ```
  pixel_size = 0.00241228  # Pixel size DJI FC6310R camera
  
  EastCoord = 416330.201661
  NorthCoord = 5913901.160997
  Altitude = 130.880995
  Omega = 0.500470
  Phi = -0.101478
  Kappa = 165.887254
  ```
  </details>
  <details>
  <summary><b><u>Parameters Example Set 2</u></b></summary>
  
  ```
  pixel_size = 0.00375  # Pixel size AXIS Q1645 camera
  
  EastCoord = 36.6563934
  NorthCoord = 102.8422117
  Altitude = 512.6745722
  Omega = -2.164596
  Phi = 60.412249
  Kappa = 93.2947178
  ```


* Run the code. Run the code. Messages with the information process will be displayed in the console


* Check the results saved in the output folder

</details>

<details>
<summary><b><u>How to export OpenCV calibration file from Metashape</u></b></summary>

</details>

<details>
<summary><b><u>How to change Rotation Angles in Metashape</u></b></summary>

</details>
<details>
<summary><b><u>Files and structure of Examples folder</u></b></summary>

```
3DPC_into_2DIMG/
  └─── Examples        
        └─── Set 1 (Drone image)
        |     └─── input_data
        |     |       └─── 100_0002_0089.png
        |     |       └─── PointCloud_light.txt
        |     |       └─── camera_calib_OpenCV.xml
        |     └─── output_data
        |             └─── Img1_100_0002_0089.png
        |             └─── Img2_100_0002_0089.png
        |             └─── 100_0002_0089.csv             
        └─── Set 2 (Terrestrial camera)
              └─── input_data
              |       └─── image22-02-07_14-15-00-34_00058.jpg
              |       └─── PointCloud_light.txt
              |       └─── camera_calib_OpenCV.xml
              └─── output_data
                      └─── Img1_image22-02-07_14-15-00-34_00058.jpg
                      └─── Img2_image22-02-07_14-15-00-34_00058.jpg
                      └─── image22-02-07_14-15-00-34_00058.csv 

```
</details>

***
<a name="citation"></a>
# Citation
Papers using this project (Please contact the authors in case of use in publications):
- Currently, no paper is published finally

If you use this code or the results for your research, please cite as:

```
@misc{3Dinto2D_light2022,
  author = {Blanch, X. & Eltner, A.},
  title = {3D Point Cloud reprojection into a 2D image},
  year = {2022},
  publisher = {GitLab},
  journal = {GitLab repository},
  howpublished = {\url{https://gitlab.com/xabierblanch/3d_pointcloud_into_2d_image}},
}
```
And provide/share the GitLab link!

- [ ]  Contact: [Xabier Blanch, TU Dresden](https://xabierbg.eu)

- [ ] Contact: [Anette Eltner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/geosensorsysteme/die-professur/Anette_Eltner)
