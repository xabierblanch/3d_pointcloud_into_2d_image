# Copyright (c) 2022, Anette Eltner & Xabier Blanch (GSS - TU Dresden)
# All rights reserved.

import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import os
from pathlib import Path
import warnings
import time
from datetime import timedelta

warnings.filterwarnings("ignore")

def ptCloud_transformations(ptCloud_file):
# File PTCloud transformation
    try:
        ptCloud = np.asarray(pd.read_table(ptCloud_file, header=None, delimiter='\s+'))  # read point cloud    ,
        # delimiter=','
        ptCloud[:,[0,1,2]] = ptCloud[:,[0,1,2]]
        print('Point cloud: "' + str(Path(ptCloud_file).name) + '" loaded -> ' + str(len(ptCloud)) + ' points processed')
    except:
        print('Failed reading point cloud file')
    return ptCloud

def getExternalOrientation_Mat(exteriorApprox):
    calib_results = exteriorApprox
    position = calib_results[0:3]
    rot_mat = rot_Matrix(calib_results[3, 0], calib_results[4, 0], calib_results[5, 0], 'radians').T
    multipl_array = np.array([[1, 1, 1], [-1, -1, -1], [-1, -1, -1]])
    rot_mat = rot_mat * multipl_array
    eor_mat = np.hstack((rot_mat.T, position.reshape(position.shape[0],
                                                     1)))  # if rotation matrix received from opencv transpose rot_mat
    eor_mat = np.vstack((eor_mat, [0, 0, 0, 1]))
    eor_mat[0:3, 3] = eor_mat[0:3, 3]
    print('\nExterior orientation matrix: ')
    print(eor_mat)
    return eor_mat

''' Umrechnung Winkel in Rotationsmatrix '''
def rot_Matrix(omega, phi, kappa, unit='grad'):  # radians
    # unit: rad = radians, gon, grad
    # gon to radian
    if unit == 'gon':
        omega = omega * (math.pi / 200)
        phi = phi * (math.pi / 200)
        kappa = kappa * (math.pi / 200)

    # grad to radian
    elif unit == 'grad':
        omega = omega * (math.pi / 180)
        phi = phi * (math.pi / 180)
        kappa = kappa * (math.pi / 180)

    # radian
    elif unit == 'radians':
        omega = omega
        phi = phi
        kappa = kappa

    r11 = math.cos(phi) * math.cos(kappa)
    r12 = -math.cos(phi) * math.sin(kappa)
    r13 = math.sin(phi)
    r21 = math.cos(omega) * math.sin(kappa) + math.sin(omega) * math.sin(phi) * math.cos(kappa)
    r22 = math.cos(omega) * math.cos(kappa) - math.sin(omega) * math.sin(phi) * math.sin(kappa)
    r23 = -math.sin(omega) * math.cos(phi)
    r31 = math.sin(omega) * math.sin(kappa) - math.cos(omega) * math.sin(phi) * math.cos(kappa)
    r32 = math.sin(omega) * math.cos(kappa) + math.cos(omega) * math.sin(phi) * math.sin(kappa)
    r33 = math.cos(omega) * math.cos(phi)

    rotMat = np.array(((r11, r12, r13), (r21, r22, r23), (r31, r32, r33)))
    return rotMat

def project_pts_into_img(eor_mat, calibration_path, point_cloud, pixel_size, image, output_folder, plot_results=False):
    # point cloud including RGB

    point_cloudXYZ = point_cloud[:, 0:3]
    point_cloudRGB = point_cloud[:, 3:6]

    '''transform point cloud into camera coordinate system'''
    point_cloud = np.matrix(np.linalg.inv(eor_mat)) * np.matrix(
        np.vstack((point_cloudXYZ.T, np.ones(point_cloudXYZ.shape[0]))))
    point_cloud = point_cloud.T

    point_cloud = np.hstack((point_cloud[:, 0:3], point_cloudXYZ, point_cloudRGB))  # reunite transformed point cloud with RGB

    '''remove points behind the camera'''
    df_points = pd.DataFrame(point_cloud)
    df_points = df_points.loc[df_points[2] > 0]
    point_cloud = np.asarray(df_points)
    del df_points

    '''inbetween coordinate system'''
    x = point_cloud[:, 0] / point_cloud[:, 2]
    y = point_cloud[:, 1] / point_cloud[:, 2]
    d = point_cloud[:, 2]

    ptCloud_img = np.hstack((x.reshape(x.shape[0], 1), y.reshape(y.shape[0], 1)))
    ptCloud_img = np.hstack((ptCloud_img, d.reshape(d.shape[0], 1)))
    if not ptCloud_img.shape[0] > 0:  # take care if img registration already erroneous
        return None

    ptCloud_img = np.hstack((ptCloud_img, point_cloudXYZ, point_cloudRGB))
    del point_cloudRGB
    del point_cloudXYZ

    '''calculate depth map but no interpolation (solely for points from point cloud'''
    cv_file = cv2.FileStorage(calibration_path, cv2.FILE_STORAGE_READ)
    cameraMatrix = cv_file.getNode("Camera_Matrix").mat()

    ptCloud_img_x = ptCloud_img[:, 0] * cameraMatrix[0, 0] * pixel_size
    ptCloud_img_y = ptCloud_img[:, 1] * -cameraMatrix[1, 1] * pixel_size
    ptCloud_img_proj = np.hstack(
        (ptCloud_img_x.reshape(ptCloud_img_x.shape[0], 1), ptCloud_img_y.reshape(ptCloud_img_y.shape[0], 1)))
    ptCloud_img_px = metric_to_pixel(ptCloud_img_proj, calibration_path, pixel_size)
    ptCloud_img_px = np.hstack((ptCloud_img_px, ptCloud_img[:, 2:9]))

    if ptCloud_img_px is None:
        print('\nPoint projection into image failed')
    else:
        print('\nPoint cloud with ' + str(ptCloud_img_px.shape[0]) + ' points projected into 2D space (image)')

    if plot_results:
        check_output_folder(output_folder)
        if point_cloud.shape[1] > 3:
            rgb = ptCloud_img[:, 6:9]
        dpi = 200
        fig = plt.figure(dpi=dpi)
        ax = fig.gca()
        ax.scatter([x], [-y], s=1, edgecolor=None, lw=0, facecolors=rgb)
        plt.title('3D point cloud in ' + str(Path(image).name) + ' space')
        plt.xticks([])
        plt.yticks([])
        try:
            plt.savefig(os.path.join(output_folder, 'Img1_' + Path(image).stem + '.png'),
                bbox_inches='tight',
                pad_inches=0.1,
                format='png',
                dpi=dpi)
            print('\nImage 1 saved in -> ' + str(os.path.join(Path(output_folder).stem, 'Img1_' + Path(image).stem +
                                                              '.png')))
        except:
            print("\nERROR: Image 1 can't be saved")
        # plt.show()
        del ax

    return ptCloud_img_px

def metric_to_pixel(img_pts_mm, calibration_path, pixel_size):
    '''
    img_mm_pts = numpy array or list with X and Y values in pixels
    '''
    cv_file = cv2.FileStorage(calibration_path, cv2.FILE_STORAGE_READ)
    image_Width = cv_file.getNode("image_Width").real()
    image_Height = cv_file.getNode("image_Height").real()

    img_pts_mm = np.array(img_pts_mm)
    center_x = image_Width / 2
    center_y = image_Height / 2
    img_pts_pix_x = img_pts_mm[:, 0] / pixel_size + np.ones(img_pts_mm.shape[0]) * (center_x - .5)
    img_pts_pix_y = image_Height - (img_pts_mm[:, 1] / pixel_size + np.ones(img_pts_mm.shape[0]) * (center_y + .5))
    img_pts_pix = np.stack((img_pts_pix_x, img_pts_pix_y), axis=-1)
    return img_pts_pix

def opencv_undistort_images(img, calibration_file):
    # read image
    img = cv2.imread(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # read calibratrion parameters from XML file
    cv_file = cv2.FileStorage(calibration_file, cv2.FILE_STORAGE_READ)
    # undistort parameters
    cameraMatrix = cv_file.getNode("Camera_Matrix").mat()
    distCoeffs = cv_file.getNode("Distortion_Coefficients").mat()
    newcameramtx = cameraMatrix
    # undistort function
    undistort_image = cv2.undistort(img, cameraMatrix, distCoeffs, None, newcameramtx)
    return undistort_image

def pointcloud_image_stacking(ptCloud_img_px, image, calibration_path, output_folder, plot_results=True):
    if plot_results:
        undistort_image = opencv_undistort_images(image, calibration_path)
        dpi=100
        rgb = ptCloud_img_px[:, 6:9]
        fig = plt.figure(figsize=(undistort_image.shape[1]/dpi, undistort_image.shape[0]/dpi), dpi=dpi)
        ax = fig.add_axes([0, 0, 1, 1])
        ax.scatter(ptCloud_img_px[:, 0], ptCloud_img_px[:, 1], s=0.5, color=rgb)
        ax.axis('off')
        ax.imshow(undistort_image, alpha=1)
        try:
            plt.savefig(os.path.join(output_folder, 'Img2_' + Path(image).stem + '.png'),
                bbox_inches='tight',
                pad_inches=0,
                format='png',
                dpi=dpi)
            print('\nImage 2 saved in -> ' + str(os.path.join(Path(output_folder).stem, 'Img2_' + Path(image).stem +
                                                              '.png')))
        except:
            print("\nERROR: Image 2 can't be saved")
        # plt.show()

def Metashape_to_ExteriorApprox(EastCoord,NorthCoord,Altitude,Omega,Phi,Kappa):
    Omega_rad = math.radians(Omega)
    Phi_rad = math.radians(Phi)
    Kappa_rad = math.radians(Kappa)
    exteriorApprox=np.array([[EastCoord],[NorthCoord],[Altitude],[Omega_rad],[Phi_rad],[Kappa_rad]])

    print(' \nEstimated camera coordinates:')
    print('East: ' + str(round(exteriorApprox[0][0],2)) + ' m')
    print('North: '  + str(round(exteriorApprox[1][0],2)) + ' m')
    print('Altitude: '  + str(round(exteriorApprox[2][0],2)) + ' m')

    print(' \nEstimated camera angles:')
    print('Omega (ω): '  + str(round(exteriorApprox[3][0],4)) + ' rad')
    print('Phi (φ): '  + str(round(exteriorApprox[4][0],4)) + ' rad')
    print('Kappa (κ): '  + str(round(exteriorApprox[5][0],4)) + ' rad')
    return exteriorApprox

def check_output_folder(output_folder):
    if not os.path.exists(output_folder):
        os.mkdir((output_folder))

def save_ptCloud_img_px(output_folder, image, ptCloud_img_px, enabled=True):
    if enabled == True:
        check_output_folder(output_folder)  # check if the output_folder is created
        img = cv2.imread(image)
        ptcloud_img_px_only = ptCloud_img_px[np.where((ptCloud_img_px[:, 0] > 0) * (ptCloud_img_px[:, 1] > 0))]
        ptcloud_img_px_only = ptcloud_img_px_only[np.where((ptcloud_img_px_only[:, 0] < img.shape[0]) * (ptcloud_img_px_only[:, 1] < img.shape[1]))]
        try:
            np.savetxt(os.path.join(output_folder, Path(image).stem + '.csv'), ptcloud_img_px_only, delimiter=',',
                       fmt= '%1.2f')
            print('\nFile with 3D and 2D points saved -> ' + str(os.path.join(Path(output_folder).stem,
                                                                             Path(image).stem + '.csv')))
        except:
            print("\nERROR: File can't be saved")

def start():
    print('\nCopyright (c) 2022, Anette Eltner & Xabier Blanch (GSS - TU Dresden)')
    print('All rights reserved\n')
    start_time = time.time()
    return start_time

def end(start_time):
    elapsed_time_secs = time.time() - start_time
    print("\nScript finished, execution took: %s secs" % round(elapsed_time_secs))


