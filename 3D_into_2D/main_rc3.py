# Copyright (c) 2022, Xabier Blanch (GSS - TU Dresden)
# All rights reserved.

import numpy as np
from pathlib import Path
import sys

sys.path.append('./bin')
import GSS_3Dinto2D as GSS

t = GSS.start()
np.set_printoptions(suppress=True)
root_path = Path(__file__).parent

''' CHANGE INPUT/OUTPUT FOLDER. By default are using the Example Set 1 '''
input_folder = root_path.parent.__str__() + r'\Examples\set 1\input_data'
output_folder = root_path.parent.__str__() + r'\Examples\set 1\output_data/'

''' Input/Output in the main folder "3D_into_2D"'''
# input_folder = root_path.__str__() + r'\input_data'
# output_folder = root_path.__str__() + r'\output_data/'

''' ---------------------- INPUT FILES ---------------------- '''
# reference files
ptCloud_path = input_folder + r'\PointCloud_light.txt'
calibration_path = input_folder + r'\camera_calib_OpenCV.xml'
# input data
image = input_folder + r'\100_0002_0089.JPG'

''' ----------- INPUT PARAMETERS (check examples)----------- '''
# input parameters (direct from Metashape, no transformation)
pixel_size = 0.00241228  # Pixel size DJI FC6310R camera

EastCoord = 416330.201661
NorthCoord = 5913901.160997
Altitude = 130.880995
Omega = 0.500470
Phi = -0.101478
Kappa = 165.887254

''' ---------------------- MAIN SCRIPT ---------------------- '''
# File transformations functions
ptCloud = GSS.ptCloud_transformations(ptCloud_path)  # transform PointCloud to the correct format

# Get exterior Camera Geometry (solvePNPRansac) #Angles always in RADIANS
exteriorApprox = GSS.Metashape_to_ExteriorApprox(EastCoord,NorthCoord,Altitude,Omega,Phi,Kappa)

# Compute External Orientation Matrix
eor_mat = GSS.getExternalOrientation_Mat(exteriorApprox)

# Reproject 3D points into 2D space
ptCloud_img_px = GSS.project_pts_into_img(eor_mat, calibration_path, ptCloud, pixel_size, image, output_folder,
                                          plot_results=True)

# PointCloud in image space and real image stacked
GSS.pointcloud_image_stacking(ptCloud_img_px, image, calibration_path, output_folder, plot_results=True)

# Save 3D projected points into 2D image (only the points visibles in the 2D image)
GSS.save_ptCloud_img_px(output_folder, image, ptCloud_img_px, enabled=True)

GSS.end(t)